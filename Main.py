def Greaterornot():
    num1 = int(input('Give a number:\n>>'))
    num2 = int(input('Give me another number:\n>>'))

    if num1 > num2:
        print(f'{num1} is greater than {num2}')
    else:
        print(f'{num2} greater than {num1}')

def rangeByUser():
    num1 = int(input('Give a number:\n>>'))
    limit = int(input('Give me a limit:\n>>'))

    if num1 < limit:
        print(f'{num1} is within the permited range')
    else:
        print(f'{num1} is out of range!')

def variableRange():
    botLim = int(input('Give the begining of the limit:\n>>'))
    topLim = int(input('Give me the top of the limit:\n>>'))
    num1 = int(input('Give me anumber to compare:\n>>'))

    if num1 < topLim and num1 > botLim:
        print(f'{num1} is wihtin range!')
    elif num1 > topLim:
        print(f'{num1} is greater than the set')
    elif num1 < botLim:
        print(f'{num1} is less than the set')

def iLikeTurtles():
    animal = str(input('What\'s your favourite animal?\n>>'))
    if animal.upper() == 'TORTUGA' or animal.upper() == 'TURTLE':
        print(f'I like turtles too!')
    else:
        print(f'{animal} is great, but I prefer turtles :(')
    
def haveNiceDay():
    raining = str(input('Is it raining?\n>>'))
    if raining.upper() == "YES":
        wind = str(input('Is it windy?\n>>'))
        if wind.upper() == "YES":
            print(f'Too windy to use an umbrella xc')
        else:
            print('Take an umbrella with you, stay safe!')
    else:
        print('Nice day isn\'t it?')

def Age():
    age = int(input('Type in your age (don\'t be shy):\n>>'))
    if age >= 30:
        print('Never is too late to learn, what course shall we take?')
    elif age <= 29 and age >= 18:
        print('Great moment to give your career a push!')
    else:
        print('Do you know where to point your future? Im quite sure I can help you :)')

def MoreStuff():
    number = int(input('Give me a number between 1 and 6:\n>>'))
    if number == 1:
        print('Today we will learn about coding! (best topic ever!)')
    elif number == 2:
        print('How about taking a digital marketing course?')
    elif number == 3:
        print('Great day to start learning desing!')
    elif number == 4:
        print('How about learning online marketing?')
    elif number == 5:
        print('Let\'s watch a few classes on audiovisual production')
    elif number == 6:
        print('It might be a good idea to developa soft skill on platzi')
    else:
        print('Please enter a valid number!!!')

def main():
    print('1.- First Challenge\n2.- Second Challenge\n3.- Third Callenge\n4.- Fourth Challenge\n5.- Fifth Challenge\n6.- Sixth Challenge\n7.- Seventh Challenge')
    challenge = int(input('What challenge you want to run?\n>>'))

    if challenge == 1:
        Greaterornot()
    elif challenge == 2:
        rangeByUser()
    elif challenge == 3:
        variableRange()
    elif challenge == 4:
        iLikeTurtles()
    elif challenge == 5:
        haveNiceDay()
    elif challenge == 6:
        Age()
    elif challenge == 7:
        MoreStuff()
    else:
        print('Please enter a valid number!!!')

if __name__=='__main__':
    main()